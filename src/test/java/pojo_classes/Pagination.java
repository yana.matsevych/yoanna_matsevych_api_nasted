package pojo_classes;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Pagination {

    /**
                "pagination": {
     *                  "total": 2000,
     *                  "pages": 211,
     *                  "page": 1,
     *                  "limit": 5,
     *                  "links": {
     *                      "previous": null,
     *                      "current": "https://gorest.co.in/public/v1/comments?page=1",
     *                      "next": "https://gorest.co.in/public/v1/comments?page=2"
     *                  }
     *              }
     */


    private int total;
    private int pages;
    private int page;
    private int limit;
    private Links links;

}
