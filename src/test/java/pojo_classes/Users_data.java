package pojo_classes;

import lombok.Builder;

@lombok.Data
@Builder
public class Users_data {

    /**
                 {
     *             "id": 2220,
     *             "post_id": 5585,
     *             "name": "Tech Global",
     *             "email": "kathi.armstrong@hotmail.com",
     *             "body": "com.github.javafaker.Faker@77f905e3"
     *         },
     */

    private int id;
    private int post_id;
    private String name;
    private String email;
    private String body;

}
