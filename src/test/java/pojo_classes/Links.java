package pojo_classes;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Links {

    /**
                    "links": {
     *                      "previous": null,
     *                      "current": "https://gorest.co.in/public/v1/comments?page=1",
     *                      "next": "https://gorest.co.in/public/v1/comments?page=2"
     *                  }
     */

    private String previous;
    private String current;
    private String next;


}
