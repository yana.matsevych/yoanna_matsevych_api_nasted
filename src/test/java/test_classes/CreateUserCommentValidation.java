package test_classes;

import com.github.javafaker.Faker;
import com.jayway.jsonpath.JsonPath;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pojo_classes.*;
import utils.ConfigReader;

import java.util.Arrays;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class CreateUserCommentValidation {

    Response response;
    Faker faker = new Faker();

    @BeforeMethod
    public void beforeTest(){

        RestAssured.baseURI = ConfigReader.getProperty("GoRestBaseURI");


    }


    @Test
    public void validatingAddUserCommentResponseBody(){

        Links links = Links
                .builder()
                .current("https://gorest.co.in/public/v1/comments?page=1")
                .next("https://gorest.co.in/public/v1/comments?page=2")
                .build();

        Pagination pagination = Pagination
                .builder()
                .total(200)
                .pages(134)
                .page(3)
                .limit(300)
                .links(links)
                .build();

        Meta meta = Meta
                .builder()
                .pagination(pagination)
                .build();

        Users_data data1 = Users_data
                .builder()
                .id(10)
                .post_id(133)
                .name("John")
                .email(faker.internet().emailAddress())
                .body(faker.internet().domainName())
                .build();

        Users_data data2 = Users_data
                .builder()
                .id(34)
                .post_id(344)
                .name("Jane")
                .email(faker.internet().emailAddress())
                .body(faker.internet().domainName())
                .build();

        Users_data data3 = Users_data
                .builder()
                .id(243)
                .post_id(455)
                .name("Mike")
                .email(faker.internet().emailAddress())
                .body(faker.internet().domainName())
                .build();

        CreateUserComment createUserComment = CreateUserComment
                .builder()
                .code(456)
                .meta(meta)
                .data(Arrays.asList(data1, data2, data3))
                .build();


        response = RestAssured
                .given().log().all()
                .contentType(ContentType.JSON)
                .header("Authorization", ConfigReader.getProperty("GoRestToken"))
                .body(createUserComment)
                .when().post("/public-api/comments")
                .then().log().all()
                .statusCode(200)
                .extract().response();

        int actualCode = response.jsonPath().getInt("code");
        String actualMeta = response.jsonPath().getString("meta");
        String actualField = response.jsonPath().getString("data[0].field");
        String actualMessage = response.jsonPath().getString("data[0].message");

        Assert.assertEquals(actualCode, 422);
        Assert.assertEquals(actualMeta, null);
        Assert.assertEquals(actualField, "post");
        Assert.assertEquals(actualMessage, "must exist");





    }


}
